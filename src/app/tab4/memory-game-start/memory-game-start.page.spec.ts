import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MemoryGameStartPage } from './memory-game-start.page';

describe('MemoryGameStartPage', () => {
  let component: MemoryGameStartPage;
  let fixture: ComponentFixture<MemoryGameStartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MemoryGameStartPage]
    }).compileComponents();

    fixture = TestBed.createComponent(MemoryGameStartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
