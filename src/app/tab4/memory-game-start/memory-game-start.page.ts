import { Component , ViewEncapsulation} from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-memory-game-start',
  templateUrl: 'memory-game-start.page.html',
  styleUrls: ['memory-game-start.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MemoryGameStartPage {

card:any;
cards:any = [];
deck:any;

// declaring move variable
moves:any = 0;
counter:any;
stars:any;
matchedCard:any;
timer:any;

 // array for opened cards
openedCards:any = [];
second:any;
minute:any;
hour:any;
interval:any;
starRating:any;

  constructor(private alertController : AlertController) {
  }

  ngOnInit() {
    // cards array holds all cards
      this.card = document.getElementsByClassName("card");    
      this.cards = [...(this.card as any)];
      // deck of all cards in game
      this.deck = document.getElementById("card-deck");
      // declare variables for star icons
      this.stars = document.querySelectorAll(".stars ion-icon[name='star']");
      this.matchedCard = document.getElementsByClassName("match");
  }
  ionViewDidEnter(){
    this.startGame();
    this.buildMain();
  }

  shuffle(array) {
      let currentIndex = array.length, temporaryValue, randomIndex;
      while (currentIndex !== 0) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
      }
      return array;
  };


// function to start a new play 
startGame(){
    // empty the openCards array
    this.openedCards = [];

    // shuffle deck
    this.cards = this.shuffle(this.cards);
    // remove all exisiting classes from each card
    for (let i = 0; i < this.cards.length; i++){
        [].forEach.call(this.cards, (item)=> {
          this.deck.appendChild(item);
        });
        this.cards[i].classList.remove("show", "open", "match", "disabled");
    }
    // reset moves
    this.moves = 0;
    this.counter = this.moves;
    // reset rating
    for (let i= 0; i < this.stars.length; i++){
        this.stars[i].style.color = getComputedStyle(document.documentElement).getPropertyValue('--color-primary-light');
        this.stars[i].style.visibility = "visible";
    }
    //reset timer
    this.second = 0;
    this.minute = 0; 
    this.hour = 0;
    this.timer = "0 mins 0 secs";
    clearInterval(this.interval);

}


// toggles open and show class to display cards
displayCard(event){
    event.target.classList.toggle("open");
    event.target.classList.toggle("show");
    event.target.classList.toggle("disabled");
};


// add opened cards to OpenedCards list and check if cards are match or not
cardOpen(event) {
  this.openedCards.push(event.target);
    let len = this.openedCards.length;
    if(len === 2){
        this.moveCounter();
        if(this.openedCards[0].type === this.openedCards[1].type){
          this.matched();
        } else {
          this.unmatched();
        }
    }
};


// when cards match
matched(){
  this.openedCards[0].classList.add("match", "disabled");
  this.openedCards[1].classList.add("match", "disabled");
  this.openedCards[0].classList.remove("show", "open", "no-event");
  this.openedCards[1].classList.remove("show", "open", "no-event");
  this.openedCards = [];
  console.log(this.matchedCard);
  this.congratulations(this.matchedCard)
}


// description when cards don't match
unmatched(){
  this.openedCards[0].classList.add("unmatched");
  this.openedCards[1].classList.add("unmatched");
  this.disable();
    setTimeout(()=>{
      this.openedCards[0].classList.remove("show", "open", "no-event","unmatched");
      this.openedCards[1].classList.remove("show", "open", "no-event","unmatched");
      this.enable();
      this.openedCards = [];
    },1100);
}


// disable cards temporarily
disable(){
    Array.prototype.filter.call(this.cards, (card)=>{
        card.classList.add('disabled');
    });
}


// enable cards and disable matched cards
enable(){
    Array.prototype.filter.call(this.cards, (card)=>{
        card.classList.remove('disabled');
        for(let i = 0; i < this.matchedCard.length; i++){
          this.matchedCard[i].classList.add("disabled");
        }
    });
}


// count player's moves
moveCounter(){
  this.moves++;
  this.counter =this.moves;
    //start timer on first click
    if(this.moves == 1){
      this.second = 0;
      this.minute = 0; 
      this.hour = 0;
      this.startTimer();
    }
    // setting rates based on moves
    if (this.moves > 8 && this.moves < 12){
        for( let i= 0; i < 3; i++){
            if(i > 1){
              this.stars[i].style.color = getComputedStyle(document.documentElement).getPropertyValue('--color-dark-2');
              this.stars[i].style.visibility = "visible";
            }
        }
    }
    else if (this.moves > 13){
        for( let i= 0; i < 3; i++){
            if(i > 0){
              this.stars[i].style.color = getComputedStyle(document.documentElement).getPropertyValue('--color-dark-2');
              this.stars[i].style.visibility = "visible";
            }
        }
    }
}


// game timer
startTimer(){
    this.interval = setInterval(()=>{
        this.timer= this.minute +" mins "+ this.second +" secs";
        this.second++;
        if(this.second == 60){
            this.minute++;
            this.second=0;
        }
        if(this.minute == 60){
            this.hour++;
            this.minute = 0;
        }
    },1000);
}


// congratulations when all cards match, show modal and moves, time and rating
congratulations(matchedcards){
  if(matchedcards != undefined){
    if (matchedcards.length == 16){
        clearInterval(this.interval);
        let finalTime = this.timer;
        this.presentAlertConfirm(this.moves,finalTime);
    };
  }
}

buildMain(){
// loop to add event listeners to each card
for (let i = 0; i < this.cards.length; i++){
  this.card = this.cards[i];
  this.card.addEventListener("click", this.displayCard.bind(this));
  this.card.addEventListener("click", this.cardOpen.bind(this));
};
}

async presentAlertConfirm(moves:any, time:any) {

  let message = `<p>You made <span id=finalMove> ${moves} </span> moves </p>
  <p>in <span id=totalTime> ${time} </span> </p>
  <p>Rating:  <span id=starRating></span></p>`;

  const alert = await this.alertController.create({
    cssClass: 'memory-game-confirm-message-alert',
    header: 'Congratulation!',
    backdropDismiss: false,
    subHeader: "You're a winner",
    message: message,
    buttons: [
      {
        text: 'Play Again',
        role: 'cancel',
        cssClass: 'custom-playagain-button',
        handler: (blah) => {
          this.startGame();
        }
      }, {
        text: 'Next Level',
        cssClass: 'custom-nextlevel-button',
        handler: () => {
          console.log('Confirm Okay');
        }
      },  {
        text: 'Share Results',
        cssClass: 'custom-shareresults-button',
        handler: () => {
          console.log('Confirm Okay');
        }
      }
    ]
  });

  await alert.present();

  // star rating in alert
    this.starRating = document.getElementById("starRating");
    for (let node of this.stars) this.starRating.appendChild(node);
}
}
