import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MemoryGamePage } from './memory-game/memory-game.page';
import { MemoryGameStartPage } from './memory-game-start/memory-game-start.page';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { Tab4PageRoutingModule } from './tab4-routing.module'
import { StarRatingModule } from 'angular-star-rating';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab4PageRoutingModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    StarRatingModule.forRoot()
  ],
  declarations: [MemoryGamePage, MemoryGameStartPage]
})
export class Tab4PageModule {}
