import { Component , ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./memory-game.json');

@Component({
  selector: 'app-memory-game',
  templateUrl: 'memory-game.page.html',
  styleUrls: ['memory-game.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MemoryGamePage {
  memoryGameList:any = data;
  constructor(private router: Router) {}

  navigateTo(id){
    console.log(id);
    this.router.navigate(['tabs/memory-game',id])
  }
}
