import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemoryGamePage } from './memory-game/memory-game.page';
import { MemoryGameStartPage } from './memory-game-start/memory-game-start.page';

const routes: Routes = [
  {
    path: '',
    component: MemoryGamePage,
  },
  {
    path: ':id',
    component: MemoryGameStartPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab4PageRoutingModule {}
