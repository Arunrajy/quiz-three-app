import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlashcardPage } from './flashcard/flashcard.page';
import { FlashcardStartPage } from './flashcard-start/flashcard-start.page';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { Tab3PageRoutingModule } from './tab3-routing.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab3PageRoutingModule,
    HttpClientModule,
    InlineSVGModule.forRoot()
  ],
  declarations: [FlashcardPage, FlashcardStartPage]
})
export class Tab3PageModule {}
