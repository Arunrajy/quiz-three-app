import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlashcardPage } from './flashcard/flashcard.page';
import { FlashcardStartPage } from './flashcard-start/flashcard-start.page';
const routes: Routes = [
  {
    path: '',
    component: FlashcardPage,
  },
  {
    path: ':id',
    component: FlashcardStartPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
