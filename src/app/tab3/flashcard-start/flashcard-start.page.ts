import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { IonSlides, AlertController } from '@ionic/angular';
declare var require:any;
const data: any = require('./flashcard-start.json');

@Component({
  selector: 'app-flashcard-start',
  templateUrl: 'flashcard-start.page.html',
  styleUrls: ['flashcard-start.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlashcardStartPage {
  flashcardMain:any = data;
  slideOpts = {
    slidesPerView: 1,
    initialSlide: 0,
    spaceBetween: 10,
    pagination: {
      el: '.swiper-pagination-header',
      type: 'progressbar',
    },
    speed: 400
  };
  turnOver: boolean;
  showButtonOne: boolean = true;
  showButtonTwo: boolean = false;
  flashcardid: number;
  getSliderId: number;
  @ViewChild('slides',{static:false}) slides:IonSlides;
  constructor(private alertController : AlertController) {}

  ionViewDidEnter(){
    this.slides.lockSwipes(true);
    this.findSliderIndex().then((data)=>{
      this.getSliderId = data;
    });
  }
  
  prevSlide(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
    this.findEndSlider();
    this.findSliderIndex().then((data)=>{
      this.getSliderId = data;
    });
    this.cardReload(this.getSliderId,'close');
  }

  nextSlide(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
    this.findEndSlider();
    this.findSliderIndex().then((data)=>{
      this.getSliderId = data;
    }); 
    this.cardReload(this.getSliderId,'close');
  }

  cardReload(id, checkReload){
    let cardId = "slideCard"+id;
    let element = document.getElementById(cardId);
    let cardFront:any = element.querySelector("#"+element.id + " " +".flash-card-content .card-front");
    let cardBack:any = element.querySelector("#"+element.id + " " +".flash-card-content .card-back");
    if(element.id == cardId && checkReload == "open"){
    this.flashcardid = id;
    setTimeout(() => {
      cardFront.style.backfaceVisibility = "visible";
      cardBack.style.backfaceVisibility = "visible";
    }, 250);
    } else if(element.id == cardId && checkReload == "close"){
      this.flashcardid = null;
      setTimeout(() => {
        cardFront.style.backfaceVisibility = "hidden";
        cardBack.style.backfaceVisibility = "hidden"; 
      }, 250);
    }
  }

  async findEndSlider(){
    let value1 = await this.slides.isBeginning();
    let value2 = await this.slides.isEnd();
    console.log(value1, value2);
    if(value2 == true){
      this.showButtonTwo= true;
      this.showButtonOne= false;
    }else{
      this.showButtonTwo= false;
      this.showButtonOne= true;
    }
    
  }
  async findSliderIndex(){
    let value = await this.slides.getActiveIndex();
    return value;
  }
  async presentAlertConfirm() {
    let message = "You Learned New Words";
    const alert = await this.alertController.create({
      cssClass: 'flashcard-confirm-message-alert',
      header: 'Congratulation!',
      backdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'Play Again',
          role: 'cancel',
          cssClass: 'custom-playagain-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Next Level',
          cssClass: 'custom-nextlevel-button',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
