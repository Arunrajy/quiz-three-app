import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlashcardStartPage } from './flashcard-start.page';

describe('FlashcardStartPage', () => {
  let component: FlashcardStartPage;
  let fixture: ComponentFixture<FlashcardStartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlashcardStartPage]
    }).compileComponents();

    fixture = TestBed.createComponent(FlashcardStartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
