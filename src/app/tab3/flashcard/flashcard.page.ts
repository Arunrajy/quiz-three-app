import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./flashcard.json');

@Component({
  selector: 'app-flashcard',
  templateUrl: 'flashcard.page.html',
  styleUrls: ['flashcard.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlashcardPage {
  flashcardLists:any = data;
  constructor(private router: Router) {}
  
  navigateTo(id){
    console.log(id);
    this.router.navigate(['tabs/flashcard',id])
  }
}
