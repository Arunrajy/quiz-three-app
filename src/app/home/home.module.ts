import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { HomePageRoutingModule } from './home-routing.module';
import { StarRatingModule } from 'angular-star-rating';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HomePageRoutingModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    StarRatingModule.forRoot()

  ],
  declarations: [HomePage]
})
export class HomePageModule {}
