import { Component , ViewEncapsulation} from '@angular/core';
import { Router,NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabsPage {
  hideTab:boolean;
  constructor(private router:Router) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
      let url =val.url;
      let parts = url.split('/');
      console.log(parts);
      // Grab the last page url.
    const pageUrl = parts[parts.length - 1];
    console.log(pageUrl);

      if(!isNaN(Number(pageUrl))){
        this.hideTab = true;
      }else{
        this.hideTab = false;
      }
    }
    })
  }

}
