import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LetterTracingStartPage } from './letter-tracing-start.page';

describe('LetterTracingStartPage', () => {
  let component: LetterTracingStartPage;
  let fixture: ComponentFixture<LetterTracingStartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LetterTracingStartPage]
    }).compileComponents();

    fixture = TestBed.createComponent(LetterTracingStartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
