import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./letter-tracing-start.json');

@Component({
  selector: 'app-letter-tracing-start',
  templateUrl: 'letter-tracing-start.page.html',
  styleUrls: ['letter-tracing-start.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LetterTracingStartPage {
  letterTracingData:any = data;

  
  /* Get container elements */
container:any;
charscontainer:any;

  /* Get buttons */
startbutton:any;
infobutton:any;
installbutton:any;
winbutton:any;
reloadbutton:any;
soundbutton:any;
errorbutton:any;

  /* Get sounds */
winsound:any;
errorsound:any;

  /* Prepare canvas */
c:any;
cx:any;
letter:any = null;
fontsize:any = 300;
paintcolour:any = [240, 240, 240];
textcolour:any = [52, 43, 56];
xoffset:any = 0;
yoffset:any = 0;
linewidth:any = 20;
pixels:any = 0;
letterpixels:any = 0;

  /* Mouse and touch events */
mousedown:any = false;
touched:any = false;
oldx:any = 0;
oldy:any = 0;

  /* Overall game presets */
state = 'intro';
sound = true;
currentstate:any;

  constructor(private router: Router) {}

  ngOnInit(){
    /* Get container elements */
    this.container = document.querySelector('#container');
    this.charscontainer = document.querySelector('#chars');

    /* Get buttons */
    this.startbutton = document.querySelector('#intro button');
    this.infobutton = document.querySelector('#infos');
    this.installbutton = document.querySelector('#install');
    this.winbutton = document.querySelector('#win button');
    this.reloadbutton = document.querySelector('#reload');
    this.soundbutton = document.querySelector('#sound');
    this.errorbutton = document.querySelector('#error button');

    /* Get sounds */
    this.winsound = document.querySelector('#winsound');
    this.errorsound = document.querySelector('#errorsound');

    /* Prepare canvas */
    this.c = document.querySelector('canvas');
    this.cx = this.c.getContext('2d');
}
ionViewDidEnter(){
  this.init();
  this.buildMain()
}
  init() {
    let yaxis = this.container.getBoundingClientRect().y;
    this.xoffset = this.container.offsetLeft;
    this.yoffset = yaxis;
    this.fontsize = (this.container.offsetHeight - this.yoffset) / 1.5;
    this.linewidth = (this.container.offsetHeight - this.yoffset) / 15;
    this.paintletter(null);
    this.setstate('intro');
  }

  togglesound() {
    if (this.sound) {
      this.sound = false;
      this.soundbutton.className = 'navbuttonoff';
    } else {
      this.sound = true;
      this.soundbutton.className = 'navbutton';
    }
  }

  showerror() {
    this.setstate('error');
    if (this.sound) {
      // this.errorsound.play();
    }
    // if (this.navigator.vibrate) {
    //   this.navigator.vibrate(100);
    // }
  }

  showinfo() {
    if (this.state !== 'info') {
      this.setstate('info');
    } else {
      this.setstate('play');
    }
  }

  setstate(newstate) {
    console.log(newstate);
    this.state = newstate;
    this.container.className = newstate;
    this.currentstate = this.state;
  }
  moreneeded() {
    this.setstate('play');
    this.mousedown = false;
  }
  retry(ev) {
    this.mousedown = false;
    this.oldx = 0;
    this.oldy = 0;
    this.paintletter(this.letter);
  }
  winner() {
    this.paintletter(null);
  }
  start() {
    this.paintletter(this.letter);
  }
  cancel() {
    this.paintletter(null);
  }
  paintletter(retryletter) {
    let chars = this.charscontainer.innerHTML;
    this.letter = retryletter || chars[Math.floor(Math.random() * chars.length)]; 
    this.c.width = this.container.offsetWidth;
    this.c.height = this.container.offsetHeight - this.yoffset;
    this.cx.font = 'bold ' + this.fontsize + 'px Open Sans';
    this.cx.fillStyle = 'rgb(' + this.textcolour.join(',') + ')';
    this.cx.strokeStyle = 'rgb(' + this.paintcolour.join(',') + ')';
    this.cx.shadowOffsetX = 2;
    this.cx.shadowOffsetY = 2;
    this.cx.shadowBlur = 4;
    this.cx.shadowColor = '#666';
    this.cx.textBaseline = 'baseline';
    this.cx.lineWidth = this.linewidth;
    this.cx.lineCap = 'round';
    this.cx.fillText(
      this.letter,
      (this.c.width - this.cx.measureText(this.letter).width) / 2,
      (this.c.height / 1.3)
    );
    this.pixels = this.cx.getImageData(0, 0, this.c.width, this.c.height);
    this.letterpixels = this.getpixelamount(
      this.textcolour[0],
      this.textcolour[1],
      this.textcolour[2]
    );
    this.cx.shadowOffsetX = 0;
    this.cx.shadowOffsetY = 0;
    this.cx.shadowBlur = 0;
    this.cx.shadowColor = '#333';
    this.setstate('play');
  }

  getpixelamount(r, g, b) {
    let pixels = this.cx.getImageData(0, 0, this.c.width, this.c.height);
    let all = pixels.data.length;
    let amount = 0;
    for (let i = 0; i < all; i += 4) {
      if (pixels.data[i] === r &&
          pixels.data[i+1] === g &&
          pixels.data[i+2] === b) {
        amount++;
      }
    }
    return amount;
  }

  paint(x, y) {
    let rx = x;
    let ry = y;
    let colour = this.pixelcolour(x, y);
    if( colour.r === 0 && colour.g === 0 && colour.b === 0) {
      this.showerror();
    } else {
      this.cx.beginPath();
      if (this.oldx > 0 && this.oldy > 0) {
        this.cx.moveTo(this.oldx, this.oldy);
      }
      this.cx.lineTo(rx, ry);
      this.cx.stroke();
      this.cx.closePath();
      this.oldx = rx;
      this.oldy = ry;
    }
  }

  pixelcolour(x, y) {
    let index:number = ((y * (this.pixels.width * 4)) + (x * 4));
    return {
      r:this.pixels.data[index],
      g:this.pixels.data[index + 1],
      b:this.pixels.data[index + 2],
      a:this.pixels.data[index + 3]
    };
  }

  pixelthreshold() {
    if (this.state !== 'error') {
      if (this.getpixelamount(
        this.paintcolour[0],
        this.paintcolour[1],
        this.paintcolour[2]
      ) / this.letterpixels > 0.75) {
        this.setstate('win');
       if (this.sound) {
        // this.winsound.play();
       }
      }
    }
  }

  /* Mouse event listeners */

  onmouseup(ev) {
    ev.preventDefault();
    this.oldx = 0;
    this.oldy = 0;
    this.mousedown = false;
    this.pixelthreshold();
  }
  onmousedown(ev) {
    ev.preventDefault();
    this.mousedown = true;
  }
  onmousemove(ev) {
    ev.preventDefault();
    if (this.mousedown) {
      this.paint(ev.clientX, ev.clientY - this.yoffset);
      ev.preventDefault();
    }
  }

  /* Touch event listeners */

  ontouchstart(ev) {
    this.touched = true;
  }
  ontouchend(ev) {
    this.touched = false;
    this.oldx = 0;
    this.oldy = 0;
    this.pixelthreshold();
  }
  ontouchmove(ev) {
    if (this.touched) {
      this.paint(
        ev.changedTouches[0].pageX,
        ev.changedTouches[0].pageY - this.yoffset
      );
      ev.preventDefault();
    }
  }

  buildMain(){

  /* Button event handlers */

  this.errorbutton.addEventListener('click', this.retry.bind(this), false);
  // this.infobutton.addEventListener('click', this.showinfo.bind(this), false);
  // this.installbutton.addEventListener('click', this.install, false);
  this.reloadbutton.addEventListener('click', this.cancel.bind(this), false);
  // this.soundbutton.addEventListener('click', this.togglesound, false);
  this.winbutton.addEventListener('click', this.winner.bind(this), false);
  this.startbutton.addEventListener('click', this.start.bind(this), false);

  /* Canvas event handlers */

  this.c.addEventListener('mouseup', this.onmouseup.bind(this), false);
  this.c.addEventListener('mousedown', this.onmousedown.bind(this), false);
  this.c.addEventListener('mousemove', this.onmousemove.bind(this), false);
  this.c.addEventListener('touchstart', this.ontouchstart.bind(this), false);
  this.c.addEventListener('touchend', this.ontouchend.bind(this), false);
  this.c.addEventListener('touchmove', this.ontouchmove.bind(this), false);

  }

}
