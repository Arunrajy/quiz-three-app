import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LetterTracingPage } from './letter-tracing.page';

describe('LetterTracingPage', () => {
  let component: LetterTracingPage;
  let fixture: ComponentFixture<LetterTracingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LetterTracingPage]
    }).compileComponents();

    fixture = TestBed.createComponent(LetterTracingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
