import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./letter-tracing.json');

@Component({
  selector: 'app-letter-tracing',
  templateUrl: 'letter-tracing.page.html',
  styleUrls: ['letter-tracing.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LetterTracingPage {
  letterTracingData:any = data;
  constructor(private router: Router) {}
  navigateTo(id){
    console.log(id);
    this.router.navigate(['tabs/letter-tracing',id])
  }
}
