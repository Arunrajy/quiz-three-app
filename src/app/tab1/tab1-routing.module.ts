import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LetterTracingPage } from './letter-tracing/letter-tracing.page';
import { LetterTracingStartPage } from './letter-tracing-start/letter-tracing-start.page';

const routes: Routes = [
  {
    path: '',
    component: LetterTracingPage,
  },
  {
    path: ':id',
    component: LetterTracingStartPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
