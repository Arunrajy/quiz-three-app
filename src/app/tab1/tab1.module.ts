import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LetterTracingPage } from './letter-tracing/letter-tracing.page';
import { LetterTracingStartPage } from './letter-tracing-start/letter-tracing-start.page';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { Tab1PageRoutingModule } from './tab1-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab1PageRoutingModule,
    HttpClientModule,
    InlineSVGModule.forRoot()
  ],
  declarations: [LetterTracingPage, LetterTracingStartPage]
})
export class Tab1PageModule {}
