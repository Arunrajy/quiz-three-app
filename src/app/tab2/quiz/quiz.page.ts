import { Component , ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./quiz-list.json');

@Component({
  selector: 'app-quiz',
  templateUrl: 'quiz.page.html',
  styleUrls: ['quiz.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuizPage {
  quizLists:any = data;
  constructor(private router: Router) {}
  navigateTo(id){
    console.log(id);
    this.router.navigate(['tabs/quiz',id])
  }
}
