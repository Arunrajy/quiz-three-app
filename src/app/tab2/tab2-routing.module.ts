import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuizPage } from './quiz/quiz.page';
import { QuizStartPage } from './quiz-start/quiz-start.page';

const routes: Routes = [
  {
    path: '',
    component: QuizPage,
  },
  {
    path: ':id',
    component: QuizStartPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
