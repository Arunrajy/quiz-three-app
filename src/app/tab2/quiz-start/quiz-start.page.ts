import { Component , ViewEncapsulation, ViewChild} from '@angular/core';
import { IonSlides, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./quiz-start.json');

@Component({
  selector: 'app-quiz-start',
  templateUrl: 'quiz-start.page.html',
  styleUrls: ['quiz-start.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuizStartPage {
  quizMain:any = data;
  slideOpts = {
    slidesPerView: 1,
    initialSlide: 0,
    spaceBetween: 10,
    pagination: {
      el: '.swiper-pagination-header',
      type: 'fraction',
    },
    speed: 400
  };
  hideNavigationConfirmButton:boolean;
  hideNavigationNextButton:boolean;
  @ViewChild('slides',{static:false}) slides:IonSlides
  constructor(private router: Router, private alertController : AlertController) {}
  ionViewDidEnter(){
    this.slides.lockSwipes(true);
    this.findSlideIndex();
  }
  confirmSlide(){
    console.log("completed");
    this.presentAlertConfirm();
    // this.router.navigate(['/tabs/home/result']);
  }
  nextSlide(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.findSlideIndex();
    this.slides.lockSwipes(true);
  }
  async findSlideIndex(){
  let value1 = await this.slides.isBeginning();
  let value2 = await this.slides.isEnd();
  console.log(value1, value2)
  if(value1 == true){
    this.hideNavigationConfirmButton  = false;
    this.hideNavigationNextButton = true
  }else if(value2 == false){
    this.hideNavigationConfirmButton  = false;
    this.hideNavigationNextButton = true
  }else if(value2 == true){
    this.hideNavigationConfirmButton  = true;
    this.hideNavigationNextButton = false
  }

  }

  async presentAlertConfirm() {
    let progressTemplate = "<ion-progress-bar value='0.5'></ion-progress-bar>";
    let message = "<div class='quiz-progress'><p><span class='progress-label'>You Scored</span><span class='quiz-answered'>5</span><span class='quiz-total'>/10</span></p>"+ progressTemplate +"</div>";
    const alert = await this.alertController.create({
      cssClass: 'quiz-confirm-message-alert',
      header: 'Congratulation!',
      backdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'Play Again',
          role: 'cancel',
          cssClass: 'custom-playagain-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Next Level',
          cssClass: 'custom-nextlevel-button',
          handler: () => {
            console.log('Confirm Okay');
          }
        },  {
          text: 'Share Results',
          cssClass: 'custom-shareresults-button',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
