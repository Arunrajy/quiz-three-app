import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuizStartPage } from './quiz-start.page';

describe('QuizStartPage', () => {
  let component: QuizStartPage;
  let fixture: ComponentFixture<QuizStartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuizStartPage],
    }).compileComponents();

    fixture = TestBed.createComponent(QuizStartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
